import Component from '@ember/component';
import { computed } from '@ember/object';
import ENV from 'metwork-frontend/config/environment';
import { inject as service } from '@ember/service';

export default Component.extend({
    tagName: 'tr',
    classNameBindings: ["isActive:table-active"],

    session: service('session'),

    isActive: computed('nodeData.fragMolExpIds', 'nodeData.fragMolExpId', function () {
        let fragMolExpIds = this.get("nodeData.fragMolExpIds")
        return this.get("nodeData.fragMolExpId") == fragMolExpIds[this.get("ionCosine.ionId")]
    }),

    actions: {
        selectAnnot(ionCosine) {
            let fragMolExpIds = this.get("nodeData.fragMolExpIds")
            this.set("nodeData.fragMolExpId", fragMolExpIds[ionCosine.ionId])
        },
        annotateIon(ionId) {
            let project = this.get("project")
            let project_id = project.get("id")
            let fragsample_id = project.get("frag_sample.id")
            let smiles = this.get("nodeData").smiles
            let access_token = this.get('session.data.authenticated.token');
            let name = 'metwork_sample_' + fragsample_id + " - " + this.get("nodeData").parentMass;
            let form_data = new FormData();
            form_data.append('ion_id', ionId);
            form_data.append('name', name);
            form_data.append('smiles', smiles);
            form_data.append('status_id', 20);
            form_data.append('db_source', 'metwork_project_' + project_id);
            form_data.append('db_id', 'ion_' + ionId);
            let APInameSpace = ""
            if (ENV.APInameSpace) {
                APInameSpace = '/' + ENV.APInameSpace
            }
            $.ajax({
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', `Token ${access_token}`);
                },
                url: ENV.host + APInameSpace + '/fragsamples/' + fragsample_id + '/add_annotation',
                dataType: 'text',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'POST',
            }).then(response => {
                this.get("reloadMetabolizationNetwork")(true)
            }, (xhr, status, error) => {
                console.log(error)
            });
        },
    }

});
