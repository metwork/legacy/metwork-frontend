import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import ENV from 'metwork-frontend/config/environment';

export default Component.extend({
    session: service('session'),

    displaySpectrum: computed("nodeData", "nodeData.fragMolExpId", "nodeData.cosine", function () {
        const nodeData = this.get("nodeData")
        if (nodeData) {
            return nodeData.cosine || this.nodeData.fragMolExpId
        }
        return false
    }),

    getSpectrumURL: computed("nodeData", "nodeData.fragMolExpId", "nodeData.cosine", function() {
        const nodeData = this.get("nodeData")
        const token = this.get('session.data.authenticated.token')
        let spectrumUpId=null

        let url = ENV.msPlotHost

        if (nodeData) { 

            if (nodeData.fragMolExpId) {
                spectrumUpId = nodeData.fragMolExpId
            }
            else if (nodeData.cosine) {
                let cosines = nodeData.cosine.split(" | ")
                    let fragMolExpIds = this.get("nodeData.fragMolExpIds")
                    let ionId = cosines[0].split(" : ")[0]
                    spectrumUpId = fragMolExpIds[ionId]
            }
            
            if (spectrumUpId) {
                url = ENV.msPlotHost + "/dash/?spectrum_up_id=mw0-" + spectrumUpId + "&spectrum_up_name=experimental"
                let spectrumDownId = null
                if (nodeData.bestAnnotation) {
                    spectrumDownId = nodeData.bestAnnotation.fragMolId
                } else if (nodeData.fragMolSimId) {
                    spectrumDownId = nodeData.fragMolSimId
                }

                if (spectrumDownId && (spectrumDownId != spectrumUpId)) {
                    url += "&spectrum_down_id=mw0-" + spectrumDownId + "&spectrum_down_name=in-silico"
                }
            }
        }

        var xhr = new XMLHttpRequest();

        xhr.open('GET', url);
        xhr.onreadystatechange = handler;
        xhr.setRequestHeader('X-Token', token);
        xhr.send();
        
        function handler() {
        if (this.readyState === this.DONE) {
            if (this.status === 200) {
            var data_url = new URL(url);
            document.querySelector('#spectrum').src = data_url;
            }
        }
        }

        return url
    }),

});
