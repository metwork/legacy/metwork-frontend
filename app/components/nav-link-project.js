import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({
    tagName: '',
    classComputed: computed('activeList', function() {
        if (this.item === this.activeList) {
            return "active"
        } else {
            return ""
        }
    }),

    actions: {
        toggleItem() {
            this.set("activeList", this.item)
        }
    }

});
