import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import CytoscapeMixin from 'metwork-frontend/mixins/cytoscape';
import { computed } from '@ember/object';
import $ from 'jquery';

export default Controller.extend( CytoscapeMixin, {
  apiStatus: service('api-status'),
  allNews: null,

  getAllNews: computed('', function() {
    let self = this
    $.ajax("https://metwork.gitlab.io/pages/metwork-news/news.json").then(function(data) {
      self.set('allNews', data);
    })
    return null
  }),

  statusColor: computed( 'apiStatus.{loading,status.available}', function() {
    if (this.get('apiStatus').loading) {
      return 'primary'
    } else if (this.get('apiStatus.status.available')) {
      return 'success'
    } else {
      return 'danger'
    }
  }),

});
