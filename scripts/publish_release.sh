#!/bin/bash
ROOT_DIR="$( cd "$( dirname "$0" )/.." >/dev/null 2>&1 && pwd )"
VERSION=$(cat $ROOT_DIR/config/environment.js | grep version | grep "[0-9.]*" -o)

# cd source/metwork-frontend

# echo $PWD

echo "publish $VERSION"

echo "$SSH_USER@$SSH_HOST:$SSH_PATH"

ls "$ROOT_DIR/builds/$VERSION/"

rsync -av --progress --del $ROOT_DIR/builds/$VERSION/ $SSH_USER@$SSH_HOST:$SSH_PATH
